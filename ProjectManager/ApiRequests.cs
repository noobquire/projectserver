﻿using Newtonsoft.Json;
using ProjectServer.DTO;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;

namespace ProjectManager
{
    public static class ApiRequests
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string apiAddress = "https://localhost:5001/api";


        public static async Task<Dictionary<Project, int>> GetTaskCount(int userId)
        {
            var response = await client.GetAsync($"{apiAddress}/users/{userId}/TaskCount");
            response.EnsureSuccessStatusCode();

            var deserializedResponse = JsonConvert.DeserializeObject<Dictionary<int, int>>(await response.Content.ReadAsStringAsync());

            return deserializedResponse.ToDictionary(kvp => new Project(Api.GetProjectAsync(kvp.Key).Result), kvp => kvp.Value);
        }

        public static async Task<IEnumerable<Task>> GetUserTasksWithNameLengthLessThan45(int userId)
        {
            var response = await client.GetAsync($"{apiAddress}/users/{userId}/TasksWithNameLengthLessThan45");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(await response.Content.ReadAsStringAsync()).Select(t => new Task(t));
        }

        public static async Task<IEnumerable<Task>> GetTasksFinishedIn2019(int userId)
        {
            var response = await client.GetAsync($"{apiAddress}/users/{userId}/TasksFinishedIn2019");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(await response.Content.ReadAsStringAsync()).Select(t => new Task(t));
        }

        public static async Task<IEnumerable<(int? Id, string Name, List<User> Participants)>> GetTeamsWithUsersOlderThan12()
        {
            var response = await client.GetAsync($"{apiAddress}/teams/WithUsersOlderThan12");
            response.EnsureSuccessStatusCode();

            var deserializedResponse = JsonConvert.DeserializeObject<IEnumerable<UsersInTeamDTO>>(await response.Content.ReadAsStringAsync());
            return deserializedResponse
                .Select(uit =>
                (uit.TeamId,
                uit.TeamName,
                uit.UsersInTeamIds.Select(id => new User(Api.GetUserAsync(id).Result)).ToList()));
        }
        public static async Task<IEnumerable<User>> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            var response = await client.GetAsync($"{apiAddress}/users/SortedByFirstNameWithTasksSortedByNameLength");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(await response.Content.ReadAsStringAsync()).Select(u => new User(u));
        }

        public static async
            Task<(User User, Project LastProject, int LastProjectTasksCount, int CancelledAndUnfinishedTasksCount, Task
                LongestTask)> GetUserStats(int userId)
        {
            var response = await client.GetAsync($"{apiAddress}/users/{userId}/stats");
            response.EnsureSuccessStatusCode();

            var deserializedResponse = JsonConvert.DeserializeObject<UserStatsDTO>(await response.Content.ReadAsStringAsync());

            return (
                new User(await Api.GetUserAsync(deserializedResponse.UserId)),
                new Project(await Api.GetProjectAsync(deserializedResponse.LastProjectId)),
                deserializedResponse.LastProjectTasksCount,
                deserializedResponse.CancelledAndUnfinishedTasksCount,
                new Task(await Api.GetTaskAsync(deserializedResponse.LongestTaskId))
                );
        }

        public static async Task<(Project Project, Task LongestTask, Task ShortestTask, int TotalUsersInProjectTeam)>
            GetProjectStats(int projectId)
        {
            var response = await client.GetAsync($"{apiAddress}/projects/{projectId}/stats");
            response.EnsureSuccessStatusCode();

            var deserializedResponse = JsonConvert.DeserializeObject<ProjectStatsDTO>(await response.Content.ReadAsStringAsync());

            return (
                new Project(await Api.GetProjectAsync(deserializedResponse.ProjectId)),
                new Task(await Api.GetTaskAsync(deserializedResponse.TaskWithLongestDescId)),
                new Task(await Api.GetTaskAsync(deserializedResponse.TaskWithShortestNameId)),
                deserializedResponse.TotalUsersInProjectTeam
                );
        }

    }
}
