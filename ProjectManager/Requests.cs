﻿using ProjectServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManager
{
    public static class Requests
    {
        public static async Task<Dictionary<Project, int>> GetTaskCount(int userId)
        {
            return (await Api.GetProjectsAsync())
                .Where(p => p.AuthorId == userId)
                .Select(p => new Project(p))
                .ToDictionary(p => p, p => p.Tasks.Count);
        }

        public static async Task<IEnumerable<Task>> GetUserTasksWithNameLengthLessThan45(int userId)
        {
            return (await Api.GetTasksAsync())
                .Where(t => t.PerformerId == userId 
                && t.Name.Length < 45)
                .Select(t => new Task(t));
        }

        public static async Task<IEnumerable<(int Id, string Name)>> GetTasksFinishedIn2019(int userId)
        {
            return (await Api.GetTasksAsync())
                .Where(t => t.PerformerId == userId 
                && t.State == TaskState.Finished 
                && t.FinishedAt.Year == 2019)
                .Select(t => (t.Id, t.Name));
        }

        public static async Task<IEnumerable<(int? Id, string Name, List<User> Participants)>>
            GetTeamsWithUsersOlderThan12()
        {
            return (await Api.GetUsersAsync())
                .Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                .OrderByDescending(u => DateTime.Now.Ticks - u.RegisteredAt.Ticks)
                .GroupBy(u => u.TeamId)
                .Select(g => (g.Key, g.Key.HasValue ? Api.GetTeamAsync(g.Key.Value).Result.Name : "",
                    g.Select(u => new User(u)).ToList()));
        }

        public static async Task<IEnumerable<User>> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            return (await Api.GetUsersAsync())
                .OrderBy(u => u.FirstName)
                .Select(u => new User(u)
                {
                    Tasks = Api.GetTasksAsync().Result
                        .Where(t => t.PerformerId == u.Id)
                        .OrderByDescending(t => t.Name.Length)
                        .Select(t => new Task(t)).ToList()
                });
        }

        public static async
            Task<(User User, Project LastProject, int LastProjectTasksCount, int CancelledAndUnfinishedTasksCount, Task
                LongestTask)> GetUserStats(int userId)
        {
            return (await Api.GetProjectsAsync())
                .Where(p => p.AuthorId == userId)
                .OrderByDescending(p => p.CreatedAt)
                .Select(p => new Project(p))
                .Select(p =>
                    (new User(Api.GetUserAsync(userId).Result),
                        p,
                        p.Tasks.Count, p.Tasks.Where(t => t.State != TaskState.Finished).Count(),
                        p.Tasks.OrderBy(t => t.CreatedAt).ThenByDescending(t => t.FinishedAt).FirstOrDefault()))
                .FirstOrDefault();
        }


        public static async Task<(Project Project, Task LongestTask, Task ShortestTask, int TotalUsersInProjectTeam)>
            GetProjectStats(int projectId)
        {
            var projectDTO = await Api.GetProjectAsync(projectId);
            var project = new Project(projectDTO);
            var longestTask = project.Tasks.OrderBy(t => t.Description.Length).Last();
            var shortestTask = project.Tasks.OrderBy(t => t.Name.Length).First();

            // "Загальна кількість користувачів в команді проекту, де або опис проекту >25 символів, або кількість тасків <3"
            var totalUsersInProjectTeam = 0;
            if (project.Description.Length > 25 || project.Tasks.Count < 3)
                totalUsersInProjectTeam = (await Api.GetUsersAsync()).Where(u => u.TeamId == projectDTO.TeamId).Count();

            return (project, longestTask, shortestTask, totalUsersInProjectTeam);
        }
    }
}