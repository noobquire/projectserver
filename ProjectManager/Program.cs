﻿using System;

namespace ProjectManager
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Task 1");
            foreach (var pair in ApiRequests.GetTaskCount(5).Result)
                Console.WriteLine($"Project {pair.Key.Name}: {pair.Value} tasks");
            Console.WriteLine("Task 2");
            Console.WriteLine("Tasks for user 3 which have name length less than 45:");
            foreach (var task in ApiRequests.GetUserTasksWithNameLengthLessThan45(3).Result) Console.WriteLine(task.Name);
            Console.WriteLine("Task 3");
            Console.WriteLine("Tasks which were finished in 2019:");
            foreach (var task in ApiRequests.GetTasksFinishedIn2019(15).Result)
                Console.WriteLine($"{task.Name}");
            Console.WriteLine("Task 4");
            foreach (var team in ApiRequests.GetTeamsWithUsersOlderThan12().Result)
                if (team.Id.HasValue)
                {
                    Console.WriteLine(
                        $"ID {team.Id}: team '{team.Name}' has following participants who are older than 12 y.o.:");
                    foreach (var user in team.Participants) Console.WriteLine($"{user.FirstName} {user.LastName}");
                }
                else
                {
                    Console.WriteLine("Users who do not have a team:");
                    foreach (var user in team.Participants) Console.WriteLine($"{user.FirstName} {user.LastName}");
                }

            Console.WriteLine("Task 6");
            var userStats = ApiRequests.GetUserStats(12).Result;
            if ((userStats.LastProject == null) | (userStats.LongestTask == null))
                Console.WriteLine("User has no projects, or project has no tasks");
            else
                Console.WriteLine($"User {userStats.User.FirstName} {userStats.User.LastName}" +
                                  $"\nLast project: {userStats.LastProject.Name}" +
                                  $"\nTasks in last project: {userStats.LastProjectTasksCount}" +
                                  $"\nUnfinished and cancelled tasks in last project: {userStats.CancelledAndUnfinishedTasksCount}" +
                                  $"\nMost durable task: {userStats.LongestTask.Name}");
            Console.WriteLine("Task 7");
            var projectStats = ApiRequests.GetProjectStats(11).Result;
            Console.WriteLine($"Project '{projectStats.Project.Name}'" +
                              $"\nTask with longest description: {projectStats.LongestTask.Name}" +
                              $"\nTask with shortest name: {projectStats.ShortestTask.Name}" +
                              $"\nTotal users in project's team: {projectStats.TotalUsersInProjectTeam}");
            Console.ReadKey();
        }
    }
}