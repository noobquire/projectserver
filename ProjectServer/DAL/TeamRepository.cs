﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectServer.DTO;

namespace ProjectServer.DAL
{
    public class TeamRepository : IRepository<TeamDTO>
    {
        private readonly List<TeamDTO> _teams = new List<TeamDTO>();

        public void Create(TeamDTO item)
        {
            if (_teams.Any(t => t.Id == item.Id)) throw new ArgumentException("Item already exists");
            _teams.Add(item);
        }

        public void Create(IEnumerable<TeamDTO> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _teams.RemoveAt(_teams.FindIndex(t => t.Id == id));
        }

        public TeamDTO Get(int id)
        {
            return _teams.Find(t => t.Id == id);
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            return _teams;
        }

        public void Update(TeamDTO item)
        {
            _teams[_teams.FindIndex(t => t.Id == item.Id)] = item;
        }
    }
}