﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectServer.DTO;

namespace ProjectServer.DAL
{
    public class TaskRepository : IRepository<TaskDTO>
    {
        private readonly List<TaskDTO> _tasks = new List<TaskDTO>();

        public void Create(TaskDTO item)
        {
            if (_tasks.Any(t => t.Id == item.Id)) throw new ArgumentException("Item already exists");
            _tasks.Add(item);
        }

        public void Create(IEnumerable<TaskDTO> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _tasks.RemoveAt(_tasks.FindIndex(t => t.Id == id));
        }

        public TaskDTO Get(int id)
        {
            return _tasks.Find(t => t.Id == id);
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            return _tasks;
        }

        public void Update(TaskDTO item)
        {
            _tasks[_tasks.FindIndex(t => t.Id == item.Id)] = item;
        }
    }
}