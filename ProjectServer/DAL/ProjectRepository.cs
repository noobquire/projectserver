﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectServer.DTO;

namespace ProjectServer.DAL
{
    public class ProjectRepository : IRepository<ProjectDTO>
    {
        private readonly List<ProjectDTO> _projects = new List<ProjectDTO>();

        public void Create(ProjectDTO item)
        {
            if (_projects.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _projects.Add(item);
        }

        public void Create(IEnumerable<ProjectDTO> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _projects.RemoveAt(_projects.FindIndex(p => p.Id == id));
        }

        public ProjectDTO Get(int id)
        {
            return _projects.Find(p => p.Id == id);
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            return _projects;
        }

        public void Update(ProjectDTO item)
        {
            _projects[_projects.FindIndex(p => p.Id == item.Id)] = item;
        }
    }
}