﻿using System.Collections.Generic;

namespace ProjectServer.DAL
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Create(IEnumerable<T> items);
        void Update(T item);
        void Delete(int id);
    }
}