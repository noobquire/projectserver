﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectServer.DTO;

namespace ProjectServer.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectRepository _projects = new ProjectRepository();
        private TaskRepository _tasks = new TaskRepository();
        private TeamRepository _teams = new TeamRepository();
        private UserRepository _users = new UserRepository();

        public IRepository<ProjectDTO> Projects => _projects;

        public IRepository<TaskDTO> Tasks => _tasks;

        public IRepository<TeamDTO> Teams => _teams;

        public IRepository<UserDTO> Users => _users;

        public void SaveChanges()
        {
            return;
        }
    }
}
