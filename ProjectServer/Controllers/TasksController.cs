﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DTO;
using ProjectServer.DAL;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public TasksController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_unitOfWork.Tasks.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_unitOfWork.Tasks.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO value)
        {
            _unitOfWork.Tasks.Create(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TaskDTO> values)
        {
            _unitOfWork.Tasks.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO value)
        {
            _unitOfWork.Tasks.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }
    }
}