﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DTO;
using ProjectServer.DAL;
using ProjectServer.Services;
using System;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectsService _projectsService;

        public ProjectsController(IUnitOfWork unitOfWork, IProjectsService projectsService)
        {
            _unitOfWork = unitOfWork;
            _projectsService = projectsService;
        }

        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_unitOfWork.Projects.GetAll());
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_unitOfWork.Projects.Get(id));
        }

        // POST: api/Projects
        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO value)
        {
            try
            {
                _unitOfWork.Projects.Create(value);
                _unitOfWork.SaveChanges();
                return Ok();
            } catch (ArgumentException ex)
            {
                return StatusCode(500);
            }
            
            
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<ProjectDTO> values)
        {
            _unitOfWork.Projects.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Projects/5
        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO value)
        {
            _unitOfWork.Projects.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // GET: api/Projects/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public ActionResult<ProjectStatsDTO> GetStats(int id)
        {
            return Ok(_projectsService.GetProjectStats(id));
        }
    }
}