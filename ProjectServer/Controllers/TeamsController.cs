﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DTO;
using ProjectServer.DAL;
using ProjectServer.Services;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITeamsService _teamsService;

        public TeamsController(IUnitOfWork unitOfWork, ITeamsService teamsService)
        {
            _unitOfWork = unitOfWork;
            _teamsService = teamsService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_unitOfWork.Teams.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_unitOfWork.Teams.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO value)
        {
            _unitOfWork.Teams.Create(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TeamDTO> values)
        {
            _unitOfWork.Teams.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO value)
        {
            _unitOfWork.Teams.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("WithUsersOlderThan12")]
        public ActionResult<IEnumerable<UsersInTeamDTO>> GetTeamsWithUsersOlderThan12()
        {
            return Ok(_teamsService.GetTeamsWithUsersOlderThan12());
        }
    }
}