using ProjectServer.DTO;
using System;
using System.Collections.Generic;

namespace ProjectServer.Services
{
    public interface ITeamsService
    {
        IEnumerable<UsersInTeamDTO> GetTeamsWithUsersOlderThan12();
    }
}
