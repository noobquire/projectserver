﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectServer.DAL;
using ProjectServer.DTO;

namespace ProjectServer.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IUnitOfWork _unitOfWork;
        public TeamsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        IEnumerable<UsersInTeamDTO> ITeamsService.GetTeamsWithUsersOlderThan12()
        {
            return 
                
                _unitOfWork.Users.GetAll()
                .Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                .OrderByDescending(u => DateTime.Now.Ticks - u.RegisteredAt.Ticks)
                .GroupBy(u => u.TeamId)
                .Select(g => new UsersInTeamDTO
                {
                    TeamId = g.Key,
                    TeamName = g.Key.HasValue ? _unitOfWork.Teams.Get(g.Key.Value).Name : "",
                    UsersInTeamIds = g.Select(u => u.Id),
                });
        }
    }
}
