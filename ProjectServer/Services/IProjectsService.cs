﻿using ProjectServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.Services
{
    public interface IProjectsService
    {
        ProjectStatsDTO GetProjectStats(int projectId); 
    }
}
