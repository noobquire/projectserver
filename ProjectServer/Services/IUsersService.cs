﻿using ProjectServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.Services
{
    public interface IUsersService
    {
        /// <summary>
        /// Get count of tasks per each project which was created by this user
        /// </summary>
        /// <returns>Key: project ID, value: amount of tasks</returns>
        IDictionary<int, int> GetTaskCount(int userId);
        IEnumerable<TaskDTO> GetTasksWithNameLengthLessThan45(int userId);
        IEnumerable<TaskDTO> GetTasksFinishedIn2019(int userId);
        UserStatsDTO GetUserStats(int userId);
        IEnumerable<TasksOfUserDTO> GetUsersSortedByFirstNameWithTasksSortedByNameLength();
    }
}
