﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectServer.DAL;
using ProjectServer.DTO;

namespace ProjectServer.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UsersService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IDictionary<int, int> GetTaskCount(int userId)
        {
            return _unitOfWork.Projects.GetAll()
                .Where(p => p.AuthorId == userId)
                .ToDictionary(p => p.Id, p => _unitOfWork.Tasks.GetAll()
                .Where(t => t.ProjectId == p.Id).Count());
        }

        public IEnumerable<TaskDTO> GetTasksFinishedIn2019(int userId)
        {
            return _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId
                && t.State == TaskState.Finished
                && t.FinishedAt.Year == 2019);
        }

        public IEnumerable<TasksOfUserDTO> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            return _unitOfWork.Users.GetAll()
                .OrderBy(u => u.FirstName)
                .Select(u => new TasksOfUserDTO
                {
                    UserId = u.Id,
                    TasksOfUserIds = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.PerformerId == u.Id)
                    .OrderByDescending(t => t.Name.Length)
                    .Select(t => t.Id),
                });
        }

        public UserStatsDTO GetUserStats(int userId)
        {
            return _unitOfWork.Projects.GetAll()
                .Where(p => p.AuthorId == userId)
                .OrderByDescending(p => p.CreatedAt)
                .Select(p => new UserStatsDTO
                {
                    UserId = userId,
                    LastProjectId = p.Id,
                    LastProjectTasksCount = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.ProjectId == p.Id).Count(),
                    CancelledAndUnfinishedTasksCount = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.ProjectId == p.Id && t.State != TaskState.Finished).Count(),
                    LongestTaskId = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.ProjectId == p.Id)
                    .OrderBy(t => t.CreatedAt)
                    .ThenByDescending(t => t.FinishedAt)
                    .FirstOrDefault().Id,
                }).FirstOrDefault();
        }

        public IEnumerable<TaskDTO> GetTasksWithNameLengthLessThan45(int userId)
        {
            return _unitOfWork.Tasks.GetAll().Where(t => t.PerformerId == userId && t.Name.Length < 45);
        }

    }
}
