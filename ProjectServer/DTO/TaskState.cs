﻿using System;

namespace ProjectServer.DTO
{
    [Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}